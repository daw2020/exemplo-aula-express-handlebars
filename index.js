const express = require('express')
const exphbs = require('express-handlebars')

const app = express()

app.use(express.static('public'))

app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}))

app.set('view engine', 'handlebars')

app.get("/", (req, res) => {
    res.render('content1', { nomes: ['Lidia', 'Maria', 'Adriana', 'Lucas', 'Pedro', 'Gustavo', 'Luciano', 'Carlos', 'Rafael'] })
})

app.get("/normal", (req, res) => {
    res.sendFile(__dirname + "/public/html/index.html")
})

var server = app.listen(3000, () => {
    console.log("Servidor rodando na porta " + server.address.port + " no host " + server.address().address)
})

